
---

*2017-11-02 14:43:03.110827*

# Muistikirja

Nyt testataan että millä tavalla tää mustikirjasysteemi toimii. Tää on aivan pelkkää nonsensea, ihan siis pleishoulderii vaan. Testataan että miten gitlab tan näyttää.

Noni, siinä oli eka paragraph. Toinenki laitetaan, ja tästä tulee pelkkää bullshittii. Jou, jou, katotaan mita tapahtuu. Nii joo, vois koittaa laittaa listan tähän:

* Item #1
* Item #2
* Linkinkin vois laittaa jos muistas miten se meni
* LInkki meni ehkä [näin](http://konstakurki.org/]

Noniin, oiskohan tässä tarpeeks ekaks testiks.

---

*2017-11-02 14:56:29.556946*

# Giglab flavored markdownin testausta

Kun kerta käyttää gitlabia muistkirjan rendaukseen, kannattaa varmaan opetella gitlabin oman markdown-maun kirjottaminen.

Line  
breakkeja  
saa  
ilmeisesti
laittamalla  
2 spacea rivin loppuun.

jos on monta_uderscorea_sanassa niin gfm ignooraa sen.

Urlit kai linkkaantuu itekseen: http://stallman.org. Saa nähä että haittaako toi piste tossa lopussa. Tää onki mulle hyvä koska tulee kirjotettuu varmaan typoja urleihin ja jos se on tossa näppärästi näkyvillä voi lukija tsiikata helposti että mihin tuli virhe.

Blockquoteja tehään näin:
> Jou
> jou
> jou

tai näin:

>>>
Jou
jou
jou

dssfdf jooo nyt tulee vähä turhan rumaa placeholderii
>>>

## Koodi (tää on siistii)

Inline koodia: `typewriter.py` (sen python skriptin nimi jolla mä tätä irjotan)

Koodiblokki pythonia (pitäs kai highlightittää):
```python
print('Jou, mä olen otus')

def funktio(x)
    print('Funktio syö safkaa ja paskantaa paskaa')
    return x
```

Okei, tolla funktiolla ei oo ruoansulatusta ollenkaan. Ja unohtu kaksoispiste määrittelystä (tää on tosiaan 100% unedited settiä)

Vai niin, tässä pystyy laittaa git diffejä tänne tekstin sekaan. [+ lisäys +] ja [- deletointi -].

Emojejaki voi näköjään laittaa :D mä kyllä suosin plain text hymiöita, mut kokeillaa ny kumminki. :pileofpoo: :poo: en tiiä mistä ton kakkakasan pitäs tulla, toivottavasti toinen noista toimii. Eipä paljoo muita emojija ees tarvii.

Hmm, tässä pystyy näkojään viittaamaan tiedostoihin ja jopa spesifiin riviin. Kokeillaas. [edellinen posti](doc/README.md#L15) Tai ainaki johonki kohtaan edellistä postia luulisin ton menevän

Sitten on joku ihme tehtävälistajuttu. Kokeillaas.
* [ ] Tätä ei oo tehty
* [x] Tää on tehty
  * Sub-listaakin vois kokeilla
  * [x] Tehty juttu

Jaa videoitaki vois laittaa. Tuntuis kyllä pöljältä laittaa valtavia videoitiedostoja git repoon.

Ou jea, matematiikkaa voi laittaa! $`R_{\alpha\beta}=0`$ Ja sit yhtälö erillisellä rivillä:
```math
\delta S=0
```
Ilmeisesti on syytä tsekkaa et mitä kaikkee latexii tää systeemi tukee ennenku kirjottelee kauheen monimutkasii yhtälöitä.

Ei perkule tää gitlab favored markdown on hyvä. Header id:tki tulee automaattisesti. Kokeillaas linkata edelliseen postiin: [linkki](#muistikirja). Toivottavasti toimii, en oo ihan varma ku ohje oli tältäosin vähän epäselvä.

Korostusta voi tehä moneen tapaan: *perus italics*, **bold**, **_molemmat_**, ja ~~yliviivaus~~ (en kyl tiiä mitä vittua sillä tekee)

Linkkejäki voi tehä monella tavalla. [Tän](typewriter.py) pitäs linkata tohon python koodiin jolla kirjotan tätä juttuu

Kuvia. Ei jaksa kokeilla.

Raakaa html:lää ki vois kai laittaa. No, kokeillaa vaikka html entityä ampersand: &amp;

Jaa ja sitten on tämmönen fancy collapse\fold systeemi.
<details><summary>Joo tätä ku painaa pitäs tula lisää bullshittii.</summary>Eli täs on ny sitä lisä bullshittii.</details>

Horisonttiviivaa en jaksa laittaa ku se vähä niinku merkkaa kirjotuksen vaihtumista tässä mun muistikirjassa

Ja joo sitten on vielä taulukko. Se voi ehkä olla hyödyllinen joskus.
| A | B |
|---|---|
| x | y |
| z | s |

Voi olla et meni vähä pieleen mut saa nähä

Footnoteja vois laittaa mut ne on kyllä aivan perseestä ku sit lukija joutuu pomppiin ees takasin, et en taia kokeilla

Ja sit ois vielä jotain wiki-spesifii juttuu, mut ne ei kyl oo relevantteja tähän hommaan. Tää testailu/harjotus oli siis tässä. Kokonaisfiilis on että tää gitlab flavored markdown on kyllä aivan tärkeen toimiva markup längvitsi.

Nää esimerkit ja tää juttu mitä mukailin oli siis joku gitlabin oma opassivu niitten markdown variantille.

Noni, se on sit ens kertaan. Moro.

---

*2017-11-02 20:46:43.030623*

onkoha tässä joku pielessä

---

*2017-11-02 21:06:27.810542*

kikuli
---

*2017-11-02 21:07:24.593076*

fds
---

*2017-11-02 21:15:21.492544*

dsf

---

*2017-11-02 21:16:27.599219*

dsfsf

---

*2017-11-02 21:17:01.839811*

sdfdsfs

---

*2017-11-02 21:17:04.163973*

sdfgdsfgsfdgdsg

---

*2017-11-02 21:18:27.327646*

ää

---

*2017-11-02 21:19:01.041066*

fdsgs

![audio/2017-11-02-21-19-01-035152.ogv][2017-11-02-21-19-01-035152.ogv]

![audio/2017-11-02-21-19-01-038497.ogv][2017-11-02-21-19-01-038497.ogv]

---

*2017-11-02 21:20:40.034970*

fsfg

---

*2017-11-02 21:21:34.624044*

# Jou, jou ja jou.

![audio/2017-11-02-21-21-34-617540.ogv][2017-11-02-21-21-34-617540.ogv]

![audio/2017-11-02-21-21-34-621413.ogv][2017-11-02-21-21-34-621413.ogv]

---

*2017-11-02 21:25:30.145118*

# ääää

![audio/2017-11-02-21-25-30-138540.ogv][audio/2017-11-02-21-25-30-138540.ogv]

![audio/2017-11-02-21-25-30-142469.ogv][audio/2017-11-02-21-25-30-142469.ogv]

---

*2017-11-02 21:30:01.671205*

# pänkeli pönkeli

![audio/2017-11-02-21-30-01-664777.ogv][audio/2017-11-02-21-30-01-664777.ogv]

![audio/2017-11-02-21-30-01-668602.ogv][audio/2017-11-02-21-30-01-668602.ogv]

---

*2017-11-02 21:31:14.954471*

# sdfsd

![audio/2017-11-02-21-31-14-948512.ogv][audio/2017-11-02-21-31-14-948512.ogv]

![audio/2017-11-02-21-31-14-951825.ogv][audio/2017-11-02-21-31-14-951825.ogv]

---

*2017-11-02 21:35:00.755003*

# ää

![audio/2017-11-02-21-35-00-748476.ogv](audio/2017-11-02-21-35-00-748476.ogv)

![audio/2017-11-02-21-35-00-752350.ogv](audio/2017-11-02-21-35-00-752350.ogv)

---

*2017-11-02 21:34:07.275270*

# testataan kännyllä

![audio/2017-11-02-21-34-07-230309.ogv](audio/2017-11-02-21-34-07-230309.ogv)

---

*2017-11-02 21:36:51.475831*

# Testi numero 2

![audio/2017-11-02-21-36-51-438790.ogv](audio/2017-11-02-21-36-51-438790.ogv)

---

*2017-11-02 21:39:15.361983*

# 123

![audio/2017-11-02-21-39-15-213429.ogv](audio/2017-11-02-21-39-15-213429.ogv)

![audio/2017-11-02-21-39-15-245891.ogv](audio/2017-11-02-21-39-15-245891.ogv)

![audio/2017-11-02-21-39-15-329068.ogv](audio/2017-11-02-21-39-15-329068.ogv)

---

*2017-11-02 21:45:23.139140*

Laptop testii

jee

jou

---

*2017-11-02 21:48:54.624429*

jee

---

*2017-11-02 21:49:28.750359*

fsdfs

---

*2017-11-02 21:50:03.199490*

sfdssg

---

*2017-11-02 21:52:11.802760*

# Josko nyt

![audio/2017-11-02-21-52-11-767595.ogv](audio/2017-11-02-21-52-11-767595.ogv)

---

*2017-11-02 21:58:25.850073*

jees

jou jou

---

*2017-11-02 21:55:06.380230*

# Alkaa pelittää

![audio/2017-11-02-21-55-06-343209.ogv](audio/2017-11-02-21-55-06-343209.ogv)

---

*2017-11-02 22:04:43.373528*

# Lisää testii

![audio/2017-11-02-22-04-43-336161.ogv](audio/2017-11-02-22-04-43-336161.ogv)
