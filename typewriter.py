#!/usr/bin/env python3

autocommit = True

import datetime
import subprocess

filename = 'README.md'

spchrs =   {'\\m':'—',
            '\\n':'–',
            '\\:':'“',
            '\\"':'”',
            '\\;':'‘',
            "\\'":'’',
            '\\.':'…',
            '\\q':'ä',
            '\\p':'ö',
            '\\w':'å',
            '\\Q':'ä',
            '\\P':'Ö',
            '\\W':'Å'}

def writeline(string):
    with open(filename,'a') as f:
        f.write(string)

def escape(string):
        string = string.replace('\\\\','\\')
        for i in spchrs:
            string = string.replace(i,spchrs[i])
        return string

def writedate():
    writeline('\n---\n\n*'+str(datetime.datetime.now())+'*\n\n')

def main():
    subprocess.check_output(['git','pull'])
    print('\a')
    writedate()
    line = input('')
    cmessage = escape(line)
    while True:
        if line == '\\quit':
            if autocommit == True:
                subprocess.check_output(['git','add',filename])
                subprocess.check_output(['git','commit','-m',cmessage])
            subprocess.check_output(['git','push'])
            print('\a')
            break
        elif line == '\\beep':
            print('\a')
        elif line == '\\specialcharacters':
            for i in spchrs:
                print('   {0}    {1}'.format(spchrs[i],i))
        else:
            writeline(escape(line)+'\n')
        line = input('')

if __name__ == '__main__':
    main()
