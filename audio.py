#!/usr/bin/env python3

import subprocess
import os
import datetime
import subprocess
import typewriter

def newfilename():
    fname = str(datetime.datetime.now())
    for i in [' ',':','.']:
        fname = fname.replace(i,'-')
    return fname + '.ogv'

def movefiles(files):
    listoffiles = []
    for i in files:
        fname = newfilename()
        listoffiles.append(fname)
        os.rename('../downloads/'+i,'audio/'+fname)
        subprocess.check_output(['git','add','audio/'+fname])
    return listoffiles

#print(movefiles(files))

def main():
    print(subprocess.check_output(['git','pull']))
    message = typewriter.escape(input('>--> '))
    files = os.listdir('../downloads')
    files = [f for f in files if f[-5:] == '.opus']
    files = movefiles(files)
    typewriter.writedate()
    typewriter.writeline('# '+message+'\n')
    formatstring = '\n![audio/{0}](audio/{1})\n'
    for i in files:
        typewriter.writeline(formatstring.format(i,i))
    subprocess.check_output(['git','add',typewriter.filename])
    subprocess.check_output(['git','commit','-m','[audio] '+message])
    subprocess.check_output(['git','push'])

if __name__ == '__main__':
    main()





